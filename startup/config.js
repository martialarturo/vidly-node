
const config = require('config');
// const morgan = require('morgan');
// const startupDebuger = require('debug')('app:startup');


module.exports = function () {
    if (!config.get('jwtPrivateKey')) {
        throw new Error('FATAL ERROR: jwtPrivateKey is undefined.');
    }

    // Environment
    // if (app.get('env') === 'development') {
    //     app.use(morgan('tiny'));
    //     startupDebuger('Morgan enabled.');
    // };

    // // Debugging
    // startupDebuger(`App Name: ${config.get('name')}`)
    // startupDebuger(`Mail Server: ${config.get('mail.host')}`)
    // startupDebuger(`Mail Password: ${config.get('mail.password')}`);
}