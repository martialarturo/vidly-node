
const winston = require('winston');
require('winston-mongodb'); //may need to comment for integration testing
require('express-async-errors');


module.exports = function () {
    process.on('uncaughtException', (ex) => {
        winston.error(ex.message, { meta: ex });
        process.exit(1);
    });

    process.on('unhandledRejection', (ex) => {
        throw ex;
    });

    winston.add(new winston.transports.Console({
        format: winston.format.combine(
            winston.format.colorize(), winston.format.prettyPrint()
        )
    }));
    winston.add(new winston.transports.File({
        filename: 'logfile.log',
        handleExceptions: true,
        level: 'error'
    }));
    winston.add(new winston.transports.MongoDB({ //may need to comment out for int. testing
        db: 'mongodb://localhost:27017/vidly',
        level: 'error',
        options: {
            useNewUrlParser: true,
            useUnifiedTopology: true,
        },
        metaKey: 'meta',
        handleExceptions: true
    }));
}
