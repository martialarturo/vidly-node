
const request = require('supertest');
const { User } = require('../../../models/user');
const mongoose = require('mongoose');
const bcrypt = require('bcrypt');

describe('/api/auth', () => {
    let server;
    let email;
    let clearPassword;
    let password;
    let user;
    let userId;
    let token;

    const exec = () => {
        return request(server)
            .post('/api/auth')
            .send({
                email: email,
                password: password
            });
    };

    beforeEach(async () => {
        server = require('../../../index');
        email = 'test@testy.com';
        clearPassword = '1a2b3C4d'
        const salt = await bcrypt.genSalt(10);
        password = await bcrypt.hash(clearPassword, salt);
        userId = mongoose.Types.ObjectId();

        user = new User({
            _id: userId,
            name: '12345',
            email: email,
            password: password
        });
        user.save();
        token = user.generateAuthToken()
    });

    afterEach(async () => {
        await server.close();
        await User.remove({});
    });

    it('returns 400 if email invalid', async () => {
        email = 'aa';
        const res = await exec();
        expect(res.status).toBe(400);
    });

    it('returns 400 if password invalid', async () => {
        password = '1234';
        const res = await exec();
        expect(res.status).toBe(400);
    });

    it('returns 400 if user not found', async () => {
        email = 'different@email.com';
        const res = await exec();
        expect(res.status).toBe(400);
    });

    it('returns 400 if wrong password', async () => {
        clearPassword = 'different';
        const res = await exec();
        expect(res.status).toBe(400);
    });

    it('returns 200 if valid request', async () => {
        password = clearPassword;
        const res = await exec();
        expect(res.status).toBe(200);
    });

    it('returns token if valid request', async () => {
        password = clearPassword;
        const res = await exec();
        expect(res.text).toBe(token);
    });
});