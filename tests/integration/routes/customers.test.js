
const request = require('supertest');
const { Customer } = require('../../../models/customer');
const { User } = require('../../../models/user');
const mongoose = require('mongoose');


describe('/api/customers', () => {
    let server;

    beforeEach(() => { server = require('../../../index'); });
    afterEach(async () => {
        await server.close();
        await Customer.remove({});
    });

    describe('GET /', () => {
        it('returns all customers', async () => {
            await Customer.collection.insertMany([
                { name: 'customer1' },
                { name: 'customer2' }
            ]);

            const res = await request(server).get('/api/customers');
            expect(res.status).toBe(200);
            expect(res.body.length).toBe(2);
            expect(res.body.some(c => c.name === 'customer1')).toBeTruthy();
            expect(res.body.some(c => c.name === 'customer2')).toBeTruthy();
        });
    });

    describe('GET /:id', () => {
        it('returns customer object', async () => {
            const customer = new Customer({ name: 'customer1', phone: 1 });
            await customer.save();

            const res = await request(server).get(`/api/customers/${customer._id}`);
            expect(res.status).toBe(200);
            expect(res.body).toHaveProperty('name', customer.name);
            expect(res.body).toHaveProperty('phone', customer.phone);
        });

        // it('returns 404 if customer ID invalid', async () => {
        //     const res = await request(server).get('/api/customers/1');
        //     expect(res.status).toBe(404);
        // });

        it('returns 404 if customer ID not found', async () => {
            const id = mongoose.Types.ObjectId()
            const res = await request(server).get(`/api/customers/${id}`);
            expect(res.status).toBe(404);
        });
    });

    describe('POST /', () => {
        let token;
        let name;
        let phone;
        let customer;

        const exec = () => {
            return request(server)
                .post('/api/customers')
                .set('x-auth-token', token)
                .send({ name, phone });
        };

        beforeEach(() => {
            token = new User().generateAuthToken();
            name = 'name1';
            phone = 1;
        });

        it('returns 401 if client not logged in', async () => {
            token = '';
            const res = await exec();
            expect(res.status).toBe(401);
        });

        it('returns 400 if customer name < 5 chars', async () => {
            name = '1234';
            const res = await exec();
            expect(res.status).toBe(400);
        });

        it('returns 400 if customer name > 50 chars', async () => {
            name = new Array(52).join('a');
            const res = await exec();
            expect(res.status).toBe(400);
        });

        it('returns 400 if customer phone is invalid', async () => {
            phone = 'a';
            const res = await exec();
            expect(res.status).toBe(400);
        });

        it('saves customer if it is valid', async () => {
            await exec();
            customer = await Customer.findOne({ name: name });
            console.log(customer);
            expect(customer.name).toBe(name);
        });

        it('returns customer if it is valid', async () => {
            const res = await exec();
            expect(res.body).toHaveProperty('name', name);
        });
    });

    describe('PUT /:id', () => {
        let token;
        let name;
        let phone;
        let newName;
        let customer;
        let id;

        const exec = async () => {
            return await request(server)
                .put(`/api/customers/${id}`)
                .set('x-auth-token', token)
                .send({ name: newName, phone: newPhone });
        };

        beforeEach(async () => {
            name = 'name1';
            phone = 1;
            customer = new Customer({ name: name, phone: phone });
            await customer.save();
            token = new User().generateAuthToken();
            id = customer._id;
            newName = 'updatedName';
            newPhone = 2
        })

        it('returns 401 if client not logged in', async () => {
            token = '';
            const res = await exec();
            expect(res.status).toBe(401);
        });

        it('returns 400 if name < 5 chars', async () => {
            newName = '1234';
            const res = await exec();
            expect(res.status).toBe(400);
        });

        it('returns 400 if name > 50 chars', async () => {
            newName = new Array(52).join('a');
            const res = await exec();
            expect(res.status).toBe(400);
        });

        it('returns 400 if phone is invalid', async () => {
            newPhone = 'a';
            const res = await exec();
            expect(res.status).toBe(400);
        });

        // it('returns 404 if id is invalid', async () => {
        //     id = 1;
        //     const res = await exec();
        //     expect(res.status).toBe(404);
        // });

        it('returns 404 if customer not found', async () => {
            id = mongoose.Types.ObjectId();
            const res = await exec();
            expect(res.status).toBe(404);
        });

        it('updates customer if it is valid', async () => {
            await exec();
            const updatedCustomer = await Customer.findById(customer._id);
            expect(updatedCustomer.name).toBe(newName);
        });

        it('returns updated customer if it is valid', async () => {
            const res = await exec();
            expect(res.body).toHaveProperty('_id');
            expect(res.body).toHaveProperty('name', newName);
        });
    });

    describe('DELETE /:id', () => {
        let token;
        let customer;
        let id;
        let name;

        const exec = async () => {
            return await request(server)
                .delete(`/api/customers/${id}`)
                .set('x-auth-token', token)
                .send();
        };

        beforeEach(async () => {
            customer = new Customer({ name: 'name1', phone: 1 });
            await customer.save();
            id = customer._id;
            token = new User({ isAdmin: true }).generateAuthToken();
        })

        it('returns 401 if client not logged in', async () => {
            token = '';
            const res = await exec();
            expect(res.status).toBe(401);
        });

        it('returns 403 if user is not admin', async () => {
            token = new User().generateAuthToken();
            const res = await exec();
            expect(res.status).toBe(403);
        });

        // it('returns 404 if id is invalid', async () => {
        //     id = '1';
        //     const res = await exec();
        //     expect(res.status).toBe(404);
        // });

        it('returns 404 if customer not found', async () => {
            id = mongoose.Types.ObjectId();
            const res = await exec();
            expect(res.status).toBe(404);
        });

        it('deletes the customer', async () => {
            await exec();
            const customerInDb = await Customer.findById(id);
            expect(customerInDb).toBeNull();
        });

        it('returns the customer that was deleted', async () => {
            const res = await exec();
            expect(res.body).toHaveProperty('_id', customer._id.toHexString());
            expect(res.body).toHaveProperty('name', customer.name);
        });
    });
});
