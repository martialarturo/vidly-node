
const moment = require('moment');
const request = require('supertest');
const { Rental } = require('../../../models/rental');
const { Movie } = require('../../../models/movie');
const { User } = require('../../../models/user');
const mongoose = require('mongoose');
const { Customer } = require('../../../models/customer');
const { Genre } = require('../../../models/genre');

describe('/api/rentals', () => {
    let server;
    let movie;
    let genre;
    let customerId1;
    let customerId2;

    beforeEach(() => { server = require('../../../index'); });
    afterEach(async () => {
        await server.close();
        await Customer.remove({});
        await Rental.remove({});
        await Movie.remove({});
    });

    describe('GET /', () => {
        let token;
        it('returns all rentals', async () => {
            customerId1 = mongoose.Types.ObjectId();
            customerId2 = mongoose.Types.ObjectId();
            genre = new Genre({ name: 'genre' });
            await Customer.collection.insertMany([
                { _id: customerId1, name: 'customer1', phone: '12345' },
                { _id: customerId2, name: 'customer2', phone: '54321' }
            ]);
            movie = new Movie({
                title: 'movie1',
                genre: genre,
                numberInStock: 5,
                dailyRentalRate: 2
            });
            await Rental.collection.insertMany([
                { customerId: customerId1, movieId: movie._id },
                { customerId: customerId2, movieId: movie._id }
            ]);
            token = new User().generateAuthToken();
            const res = await request(server)
                .get('/api/rentals')
                .set('x-auth-token', token)
                .send();
            expect(res.status).toBe(200);
            expect(res.body.length).toBe(2);
            expect(res.body[0]).toHaveProperty('customerId');
            expect(res.body[1]).toHaveProperty('customerId');
        });
    });

    // describe('POST /', () => {
    //     let token;
    //     const exec = () => {
    //         return request(server)
    //             .post('/api/rentals')
    //             .set('x-auth-token', token)
    //             .send({ customerId, movieId });
    //     };

    //     beforeEach(async () => {
    //         server = require('../../../index');
    //         customerId = mongoose.Types.ObjectId();
    //         movieId = mongoose.Types.ObjectId();
    //         token = new User().generateAuthToken();

    //         movie = new Movie({
    //             _id: movieId,
    //             title: '12345',
    //             dailyRentalRate: 2,
    //             genre: { name: '12345' },
    //             numberInStock: 10
    //         });
    //         await movie.save();

    //         rental = new Rental({
    //             customer: {
    //                 _id: customerId,
    //                 name: '12345',
    //                 phone: '12345'
    //             },
    //             movie: {
    //                 _id: movieId,
    //                 title: '12345',
    //                 dailyRentalRate: 2
    //             }
    //         });
    //         await rental.save();
    //     });
    //     // afterEach(async () => {
    //     //     await server.close();
    //     //     await Rental.remove({});
    //     //     await Movie.remove({});
    //     // });

    //     // it('returns 401 if client not logged in', async () => {
    //     //     token = '';
    //     //     const res = await exec();
    //     //     expect(res.status).toBe(401);
    //     // });

    //     // it('returns 400 if customerId not provided', async () => {
    //     //     customerId = '';
    //     //     const res = await exec();
    //     //     expect(res.status).toBe(400);
    //     // });

    //     // it('returns 400 if movieId not provided', async () => {
    //     //     movieId = '';
    //     //     const res = await exec();
    //     //     expect(res.status).toBe(400);
    //     // });

    //     // it('returns 404 if no rental found for customer/movie', async () => {
    //     //     await Rental.remove({});
    //     //     const res = await exec();
    //     //     expect(res.status).toBe(404);
    //     // });

    //     // it('returns 400 if rental already returned', async () => {
    //     //     rental.dateReturned = new Date();
    //     //     rental.save();
    //     //     const res = await exec();
    //     //     expect(res.status).toBe(400);
    //     // });

    //     it('returns 200 if valid request', async () => {
    //         const res = await exec();
    //         expect(res.status).toBe(200);
    //     });

    //     //     it('sets return date if input is valid', async () => {
    //     //         const res = await exec();
    //     //         const rentalInDb = await Rental.findById(rental._id);
    //     //         const diff = new Date() - rentalInDb.dateReturned;
    //     //         expect(diff).toBeLessThan(10 * 1000);
    //     //     });

    //     //     it('sets rental fee if input is valid', async () => {
    //     //         rental.dateOut = moment().add(-7, 'days').toDate();
    //     //         rental.save();
    //     //         const res = await exec();
    //     //         const rentalInDb = await Rental.findById(rental._id);
    //     //         expect(rentalInDb.rentalFee).toBe(14);
    //     //     });

    //     //     it('increases stock for movie', async () => {
    //     //         const res = await exec();
    //     //         const movieInDb = await Movie.findById(movieId);
    //     //         expect(movieInDb.numberInStock).toBe(movie.numberInStock + 1);
    //     //     });

    //     //     it('returns rental summary', async () => {
    //     //         const res = await exec();
    //     //         const rentalInDb = await Rental.findById(rental._id);

    //     //         expect(Object.keys(res.body)).toEqual(
    //     //             expect.arrayContaining([
    //     //                 'dateOut',
    //     //                 'dateReturned',
    //     //                 'rentalFee',
    //     //                 'customer',
    //     //                 'movie'
    //     //             ]));
    //     //     });
    // });
});