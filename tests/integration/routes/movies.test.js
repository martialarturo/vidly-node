
const request = require('supertest');
const { Movie } = require('../../../models/movie');
const { Genre } = require('../../../models/genre');
const { User } = require('../../../models/user');
const mongoose = require('mongoose');


describe('/api/movies', () => {
    let server;
    let genreId;
    let movieId;

    beforeEach(async () => {
        server = require('../../../index');
        genre = new Genre({ name: 'genre1' });
        genre.save();
        genreId = genre._id;
    });
    afterEach(async () => {
        await server.close();
        await Movie.remove({});
        await Genre.remove({});
    });

    describe('GET /', () => {
        it('returns all movies', async () => {
            genreId = genre._id;
            await Movie.collection.insertMany([
                { title: 'movie1', genre: genre, numberInStock: 2, dailyRentalRate: 2 },
                { title: 'movie2', genre: genre, numberInStock: 2, dailyRentalRate: 2 }
            ]);

            const res = await request(server).get('/api/movies');
            expect(res.status).toBe(200);
            expect(res.body.length).toBe(2);
            expect(res.body.some(m => m.title === 'movie1')).toBeTruthy();
            expect(res.body.some(m => m.title === 'movie2')).toBeTruthy();
        });
    });

    describe('GET /:id', () => {
        const exec = () => {
            return request(server)
                .get(`/api/movies/${movieId}`)
                .send();
        };

        beforeEach(async () => {
            movie = new Movie({ title: 'movie1', genre: genre, numberInStock: 2, dailyRentalRate: 2 });
            await movie.save();
        });

        it('returns 404 if movie ID not found', async () => {
            movieId = mongoose.Types.ObjectId()
            const res = await exec();
            expect(res.status).toBe(404);
        });

        it('returns movie object', async () => {
            const res = await request(server).get(`/api/movies/${movie._id}`);
            expect(res.status).toBe(200);
            expect(res.body).toHaveProperty('title', movie.title);
            expect(res.body).toHaveProperty('genre');
        });

    });

    describe('POST /', () => {
        let token;
        let title;

        const exec = () => {
            return request(server)
                .post('/api/movies')
                .set('x-auth-token', token)
                .send({ title, genreId, numberInStock: 2, dailyRentalRate: 2 });
        };

        beforeEach(() => {
            token = new User().generateAuthToken();
            title = '12345';
        });

        it('returns 401 if client not logged in', async () => {
            token = '';
            const res = await exec();
            expect(res.status).toBe(401);
        });

        it('returns 400 if movie title < 5 chars', async () => {
            title = '1234';
            const res = await exec();
            expect(res.status).toBe(400);
        });

        it('returns 400 if movie title > 50 chars', async () => {
            title = new Array(52).join('a');
            const res = await exec();
            expect(res.status).toBe(400);
        });

        it('returns 400 if genreId is invalid', async () => {
            genreId = 'a';
            const res = await exec();
            expect(res.status).toBe(400);
        });

        it('returns 400 if genre not found', async () => {
            genreId = mongoose.Types.ObjectId();
            const res = await exec();
            expect(res.status).toBe(400);
        });

        it('saves movie if it is valid', async () => {
            await exec();
            movie = await Movie.findOne({ title: title });
            expect(movie.title).toBe(title);
        });

        it('returns movie if it is valid', async () => {
            const res = await exec();
            expect(res.body).toHaveProperty('title', title);
        });
    });

    describe('PUT /:id', () => {
        let token;
        let newTitle;
        let genreId;
        let movieId;

        const exec = async () => {
            return await request(server)
                .put(`/api/movies/${movieId}`)
                .set('x-auth-token', token)
                .send({
                    title: newTitle,
                    genreId: genreId,
                    numberInStock: 2,
                    dailyRentalRate: 2
                });
        };

        beforeEach(async () => {
            title = 'movie1';
            movie = new Movie({
                title: title,
                genre: genre,
                numberInStock: 2,
                dailyRentalRate: 2
            });
            await movie.save();
            token = new User().generateAuthToken();
            movieId = movie._id;
            genreId = genre._id;
            newTitle = 'updatedTitle';
        })

        it('returns 401 if client not logged in', async () => {
            token = '';
            const res = await exec();
            expect(res.status).toBe(401);
        });

        it('returns 400 if title < 5 chars', async () => {
            newTitle = '1234';
            const res = await exec();
            expect(res.status).toBe(400);
        });

        it('returns 400 if title > 50 chars', async () => {
            newTitle = new Array(52).join('a');
            const res = await exec();
            expect(res.status).toBe(400);
        });

        it('returns 400 if genre is invalid', async () => {
            genreId = mongoose.Types.ObjectId();
            const res = await exec();
            expect(res.status).toBe(400);
        });

        it('returns 404 if movie not found', async () => {
            movieId = mongoose.Types.ObjectId();
            const res = await exec();
            expect(res.status).toBe(404);
        });

        it('updates movie if it is valid', async () => {
            await exec();
            const updatedMovie = await Movie.findById(movie._id);
            expect(updatedMovie.title).toBe(newTitle);
        });

        it('returns updated movie if it is valid', async () => {
            const res = await exec();
            expect(res.body).toHaveProperty('_id');
            expect(res.body).toHaveProperty('title', newTitle);
        });
    });

    describe('DELETE /:id', () => {
        let token;
        let movieId;

        const exec = async () => {
            return await request(server)
                .delete(`/api/movies/${movieId}`)
                .set('x-auth-token', token)
                .send();
        };

        beforeEach(async () => {
            movie = new Movie({
                title: 'movie1',
                genre: genre,
                numberInStock: 2,
                dailyRentalRate: 2
            });
            await movie.save();
            movieId = movie._id;
            token = new User({ isAdmin: true }).generateAuthToken();
        })

        it('returns 401 if client not logged in', async () => {
            token = '';
            const res = await exec();
            expect(res.status).toBe(401);
        });

        it('returns 403 if user is not admin', async () => {
            token = new User().generateAuthToken();
            const res = await exec();
            expect(res.status).toBe(403);
        });

        // it('returns 404 if id is invalid', async () => {
        //     id = '1';
        //     const res = await exec();
        //     expect(res.status).toBe(404);
        // });

        it('returns 404 if customer not found', async () => {
            movieId = mongoose.Types.ObjectId();
            const res = await exec();
            expect(res.status).toBe(404);
        });

        it('deletes the movie', async () => {
            await exec();
            const movieInDb = await Movie.findById(movieId);
            expect(movieInDb).toBeNull();
        });

        it('returns the movie that was deleted', async () => {
            const res = await exec();
            expect(res.body).toHaveProperty('_id', movie._id.toHexString());
            expect(res.body).toHaveProperty('title', movie.title);
        });
    });
});
