
const request = require('supertest');
const { Genre } = require('../../../models/genre');
const { User } = require('../../../models/user');
const mongoose = require('mongoose');

let server;

describe('/api/genres', () => {
    beforeEach(() => { server = require('../../../index'); });
    afterEach(async () => {
        await server.close();
        await Genre.remove({});
    });

    describe('GET /', () => {
        it('returns all genres', async () => {
            await Genre.collection.insertMany([
                { name: 'genre1' },
                { name: 'genre2' }
            ]);

            const res = await request(server).get('/api/genres');
            expect(res.status).toBe(200);
            expect(res.body.length).toBe(2);
            expect(res.body.some(g => g.name === 'genre1')).toBeTruthy();
            expect(res.body.some(g => g.name === 'genre2')).toBeTruthy();
        });
    });

    describe('GET /:id', () => {
        it('returns genre object', async () => {
            const genre = new Genre({ name: 'genre1' });
            await genre.save();

            const res = await request(server).get(`/api/genres/${genre._id}`);
            expect(res.status).toBe(200);
            expect(res.body).toHaveProperty('name', genre.name);
        });

        it('returns 404 if genre ID invalid', async () => {
            const res = await request(server).get('/api/genres/1');
            expect(res.status).toBe(404);
        });

        it('returns 404 if genre ID not found', async () => {
            const id = mongoose.Types.ObjectId()
            const res = await request(server).get(`/api/genres/${id}`);
            expect(res.status).toBe(404);
        });

    });

    describe('POST /', () => {
        let token;
        let name;

        const exec = async () => {
            return await request(server)
                .post('/api/genres')
                .set('x-auth-token', token)
                .send({ name });
        };

        beforeEach(() => {
            token = new User().generateAuthToken();
            name = 'genre1';
        });

        it('returns 401 if client not logged in', async () => {
            token = '';
            const res = await exec();
            expect(res.status).toBe(401);
        });

        it('returns 400 if genre < 5 chars', async () => {
            name = '1234';
            const res = await exec();
            expect(res.status).toBe(400);
        });

        it('returns 400 if genre > 50 chars', async () => {
            name = new Array(52).join('a');
            const res = await exec();
            expect(res.status).toBe(400);
        });

        it('saves genre if it is valid', async () => {
            await exec();
            const genre = await Genre.find({ name: 'genre1' });
            expect(genre).not.toBeNull();
        });

        it('returns genre if it is valid', async () => {
            const res = await exec();
            expect(res.body).toHaveProperty('_id');
            expect(res.body).toHaveProperty('name', 'genre1');
        });
    });

    describe('PUT /:id', () => {
        let token;
        let newName;
        let genre;
        let id;

        const exec = async () => {
            return await request(server)
                .put(`/api/genres/${id}`)
                .set('x-auth-token', token)
                .send({ name: newName });
        };

        beforeEach(async () => {
            genre = new Genre({ name: 'genre1' });
            await genre.save();
            token = new User().generateAuthToken();
            id = genre._id;
            newName = 'updatedName';
        })

        it('returns 401 if client not logged in', async () => {
            token = '';
            const res = await exec();
            expect(res.status).toBe(401);
        });

        it('returns 400 if name < 5 chars', async () => {
            newName = '1234';
            const res = await exec();
            expect(res.status).toBe(400);
        });

        it('returns 400 if name > 50 chars', async () => {
            newName = new Array(52).join('a');
            const res = await exec();
            expect(res.status).toBe(400);
        });

        it('returns 404 if id is invalid', async () => {
            id = 1;
            const res = await exec();
            expect(res.status).toBe(404);
        });

        it('returns 404 if genre not found', async () => {
            id = mongoose.Types.ObjectId();
            const res = await exec();
            expect(res.status).toBe(404);
        });

        it('updates genre if it is valid', async () => {
            await exec();
            const updatedGenre = await Genre.findById(genre._id);
            expect(updatedGenre.name).toBe(newName);
        });

        it('returns updated genre if it is valid', async () => {
            const res = await exec();
            expect(res.body).toHaveProperty('_id');
            expect(res.body).toHaveProperty('name', newName);
        });
    });

    describe('DELETE /:id', () => {
        let token;
        let genre;
        let id;

        const exec = async () => {
            return await request(server)
                .delete(`/api/genres/${id}`)
                .set('x-auth-token', token)
                .send();
        };

        beforeEach(async () => {
            genre = new Genre({ name: 'genre1' });
            await genre.save();
            id = genre._id;
            token = new User({ isAdmin: true }).generateAuthToken();
        })

        it('returns 401 if client not logged in', async () => {
            token = '';
            const res = await exec();
            expect(res.status).toBe(401);
        });

        it('returns 403 if user is not admin', async () => {
            token = new User().generateAuthToken();
            const res = await exec();
            expect(res.status).toBe(403);
        });

        it('returns 404 if id is invalid', async () => {
            id = '1';
            const res = await exec();
            expect(res.status).toBe(404);
        });

        it('returns 404 if genre not found', async () => {
            id = mongoose.Types.ObjectId();
            const res = await exec();
            expect(res.status).toBe(404);
        });

        it('deletes the genre', async () => {
            await exec();
            const genreInDb = await Genre.findById(id);
            expect(genreInDb).toBeNull();
        });

        it('returns the genre that was deleted', async () => {
            const res = await exec();
            expect(res.body).toHaveProperty('_id', genre._id.toHexString());
            expect(res.body).toHaveProperty('name', genre.name);
        });
    });
});