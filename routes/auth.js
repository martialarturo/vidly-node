
const bcrypt = require('bcrypt');
const _ = require('lodash');
const validate = require('../middleware/validate');
const { User } = require('../models/user');
const Joi = require('joi');
const express = require('express');
const router = express.Router();

// POST
router.post('/', validate(validateAuthRequest), async (req, res) => {
    // const { error } = validate(req.body);
    // if (error) return res.status(400).send(error.details[0].message);

    let user = await User.findOne({ email: req.body.email });
    if (!user) return res.status(400).send('Invalid Email or password.');

    const validPassword = await bcrypt.compare(req.body.password, user.password);
    if (!validPassword) return res.status(400).send('Invalid email or Password.');

    const token = user.generateAuthToken();
    res.send(token);
});

function validateAuthRequest(req) {
    const schema = Joi.object({
        email: Joi.string().min(5).max(255).email().required(),
        password: Joi.string().min(5).max(255).required(),
    });
    return schema.validate(req);
}


module.exports = router;