
const express = require('express');
const router = express.Router();
// HOMEPAGE
router.get('/', (req, res) => {
    res.render('index', { title: 'Vidly', message: 'Hello' });
});

module.exports = router;